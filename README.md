# Spiderweb

Spiderweb lets you do one simple thing: check if the current request comes from a robot or not.

## Installation

Add this line to your application's Gemfile:

    gem 'spiderweb'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install spiderweb

## Example usage

  # use this in a before_filter in your application controller
  # returns true or false
  Spiderweb.is_bot(request)

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
