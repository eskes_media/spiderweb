require "spiderweb/version"

module Spiderweb
  def self.is_bot(request)
    bots = ['bot','crawl','slurp','spider','spyder','data','content','creep','scooter','internet','tricus','search','download','check','ingrid','adsense','magpie','arks','askjeeves','turn it in','shinchakubin','alexa','appie','rambler','marvin','voila','netcraft','larbin','voyager','pompos','link','orlando','big brother','echo','mirago','digger','archiver','robi','patric','holmes','jemmathetourist','index','wise-guys','ilse','curl','teoma','ask','jeeves','yanga','ichiro','sargas','ndex','python','ning','facebook','scout','ruby','lipperhey','butterfly','postrank','comet','unwind','topblogs','validator','media']
    robot = bots.any? { |bot| request.env['HTTP_USER_AGENT'].include? bot }
    return robot
  end
end
