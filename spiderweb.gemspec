# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'spiderweb/version'

Gem::Specification.new do |spec|
  spec.name          = "spiderweb"
  spec.version       = Spiderweb::VERSION
  spec.authors       = ["Eskes Media, Henk Meijer"]
  spec.email         = ["meijerhenk@gmail.com"]
  spec.description   = %q{This gem lets you detect wether the incoming request comes from a spider}
  spec.summary       = %q{Logging or filtering incomming requests? Use spiderweb to determine if your dealing with a bot.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
